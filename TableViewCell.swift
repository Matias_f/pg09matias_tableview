//
//  TableViewCell.swift
//  PG09Matias_TableView
//
//  Created by Matias Felipe Fuentes on 2017-07-05.
//  Copyright © 2017 VFS. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var mLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
