//
//  TableViewCellProduct.swift
//  PG09Matias_TableView
//
//  Created by Matias Felipe Fuentes on 2017-07-19.
//  Copyright © 2017 VFS. All rights reserved.
//

import UIKit

class TableViewCellProduct: UITableViewCell {

    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productDescriptionLabel: UILabel!
    @IBOutlet weak var productRegularPriceLabel: UILabel!
    @IBOutlet weak var productCustomerRatingLabel: UILabel!
    @IBOutlet weak var productIdLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupCell(withProduct product: Product)
    {
        productNameLabel.text = product.productName
        productDescriptionLabel.text = product.productDescription
        productRegularPriceLabel.text = String(product.productRegularPrice)
        productCustomerRatingLabel.text = String(product.productCustomerRating)
        productIdLabel.text = product.productId
    }
}
