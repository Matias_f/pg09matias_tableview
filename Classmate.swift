//
//  Classmate.swift
//  PG09Matias_TableView
//
//  Created by Matias Felipe Fuentes on 2017-07-05.
//  Copyright © 2017 VFS. All rights reserved.
//

import Foundation

class Classmate {

    let name: String!
    let country: String!
    
    init(name: String, country: String) {
        self.name = name
        self.country = country
    }
}
