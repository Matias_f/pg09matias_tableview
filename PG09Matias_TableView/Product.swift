//
//  Product.swift
//  PG09Matias_TableView
//
//  Created by Matias Felipe Fuentes on 2017-07-19.
//  Copyright © 2017 VFS. All rights reserved.
//

import Foundation

class Product
{
    let productName: String!
    let productDescription: String!
    let productRegularPrice: Double!
    let productCustomerRating: Double!
    let productId: String!

    // We Initialize our Category class.
    init(productName: String, productDescription: String, productRegularPrice: Double, productCustomerRating: Double, productId: String)
    {
        self.productName = productName
        self.productDescription = productDescription
        self.productRegularPrice = productRegularPrice
        self.productCustomerRating = productCustomerRating
        self.productId = productId
    }
 
}
