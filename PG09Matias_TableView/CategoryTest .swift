//
//  CategoryTest .swift
//  PG09Matias_TableView
//
//  Created by Matias Felipe Fuentes on 2017-07-12.
//  Copyright © 2017 VFS. All rights reserved.
//

import Foundation

class CategoryTest {

    let categoryId: String!
    let categoryName: String!
    let hasSubCategories: Bool!
    let productCount: Int!
    
    // We Initialize our Category class.
    init(categoryId: String, categoryName: String, hasSubCategories: Bool, productCount: Int) {
        
        self.categoryId = categoryId
        self.categoryName = categoryName
        self.hasSubCategories = hasSubCategories
        self.productCount = productCount
    }
}
