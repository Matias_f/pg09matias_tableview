//
//  TableViewControllerTwo.swift
//  PG09Matias_TableView
//
//  Created by Matias Felipe Fuentes on 2017-07-12.
//  Copyright © 2017 VFS. All rights reserved.
//

import UIKit

class TableViewControllerTwo: UITableViewController {
    
    var currentCategory : CategoryTest!
    var productsCategory = [Product]()
    var currentId: String!
    let cellHeight = 300;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "TableViewCellProduct", bundle: nil), forCellReuseIdentifier: "product")
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        getNetworkData()

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return productsCategory.count
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(self.cellHeight)
    }
    
    private func getNetworkData()
    {
        let url = "http://www.bestbuy.ca/api/v2/json/search?categoryid="
        let langRequest = "&lang=en"
        let currentURL = URL(string: url + currentId + langRequest)
        
        let myRequest = URLRequest(url: currentURL!)
        let session = URLSession.shared
        let task = session.dataTask(with: myRequest)
        {
            (data, response, error) -> Void in
            
            if let data = data, let json = try? JSONSerialization.jsonObject(with: data, options: []), let dict = json as? [String: Any] {
                if let subCategoriesArray = dict["products"] as? [[String: Any]] {
                    for subCategory in subCategoriesArray {
                        
                        if let productName = subCategory["categoryName"] as? String,
                            let productDescription = subCategory["shortDescription"] as? String,
                            let productRegularPrice = subCategory["regularPrice"] as? Double,
                            let productCustomerRating = subCategory["customerRating"] as? Double,
                            let productId = subCategory["sku"] as? String {

                            let currentProduct = Product(productName: productName, productDescription: productDescription, productRegularPrice: productRegularPrice, productCustomerRating: productCustomerRating, productId: productId)
                            self.productsCategory.append(currentProduct)
                        }
                    }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            }
            
        }
        task.resume()
    }
 
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "product", for: indexPath) as! TableViewCellProduct

        cell.productNameLabel.text = self.productsCategory[indexPath.row].productName
        cell.productDescriptionLabel.text = self.productsCategory[indexPath.row].productDescription
        cell.productRegularPriceLabel.text = String(self.productsCategory[indexPath.row].productRegularPrice)
        cell.productCustomerRatingLabel.text = String(self.productsCategory[indexPath.row].productCustomerRating)
        cell.productIdLabel.text = self.productsCategory[indexPath.row].productId

        return cell
    }
}
