//
//  MainTableViewController.swift
//  PG09Matias_TableView
//
//  Created by Matias Felipe Fuentes on 2017-07-05.
//  Copyright © 2017 VFS. All rights reserved.
//

import UIKit

class MainTableViewController: UITableViewController {
    
    var CategoryArray = [CategoryTest]()
    var catergories = [String]()
    var classmates: [Classmate]!
    let cellHeight = 126;
    
    func loadData()
    {
        catergories.append("String # 1")
        catergories.append("String # 2")
        catergories.append("String # 3")
        catergories.append("String # 4")
        catergories.append("String # 5")
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadData()
        
        classmates = [Classmate]()
        classmates.append(Classmate(name: "Neo", country: "Thailand"))
        classmates.append(Classmate(name: "Jaxon", country: "Canada"))
        classmates.append(Classmate(name: "Hillary", country: "Venezuela"))
            
        tableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "categoryCells")
        tableView.register(UINib(nibName: "TableViewCellTwo", bundle: nil), forCellReuseIdentifier: "classmates")
        
        getNetworkData();
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    private func getNetworkData()
    {
        let myUrl = URL(string: "http://www.bestbuy.ca/api/v2/json/category/20001?lang=en&format=json")!
        let myRequest = URLRequest(url: myUrl)
        let session = URLSession.shared
        let task = session.dataTask(with: myRequest)
        {
            (data, response, error) -> Void in
            
            if let data = data, let json = try? JSONSerialization.jsonObject(with: data, options: []), let dict = json as? [String: Any] {
                if let subCategoriesArray = dict["subCategories"] as? [[String: Any]] {
                    for subCategory in subCategoriesArray {
                        
                        if let categoryId = subCategory["id"] as? String,
                            let currentName = subCategory["name"] as? String,
                            let hasSubCategories = subCategory["hasSubcategories"] as? Bool,
                            let productCount = subCategory["productCount"] as? Int {
                            
                            let category = CategoryTest(categoryId: categoryId, categoryName: currentName, hasSubCategories: hasSubCategories, productCount: productCount)
                            
                            self.CategoryArray.append(category)
                        }
                    }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            }

        }
        task.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rowz
        return CategoryArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "classmates", for: indexPath) as! TableViewCellTwo
            
        cell.mLabel.text = CategoryArray[indexPath.row].categoryName
        let currentCount = CategoryArray[indexPath.row].productCount as NSNumber
        let currentCountString: String = currentCount.stringValue
        cell.mLabelTwo.text = currentCountString
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(self.cellHeight)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let currentCategory = CategoryArray[indexPath.row]
        let vc = TableViewControllerTwo()
        vc.currentId = CategoryArray[indexPath.row].categoryId
        vc.currentCategory = currentCategory
        navigationController?.pushViewController(vc, animated: true)
    }
}
