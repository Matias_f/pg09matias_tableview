//
//  TableViewCellTwo.swift
//  PG09Matias_TableView
//
//  Created by Matias Felipe Fuentes on 2017-07-05.
//  Copyright © 2017 VFS. All rights reserved.
//

import UIKit

class TableViewCellTwo: UITableViewCell {

    @IBOutlet weak var mLabel: UILabel!
    @IBOutlet weak var mLabelTwo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupCell(withClassmate classmate: Classmate) {
    
        mLabel.text = classmate.name
        mLabelTwo.text = classmate.country
    }
    
}
